package com.vlad161.voicemessenger.presenter

import com.vlad161.voicemessenger.model.Model
import com.vlad161.voicemessenger.model.dto.MessageDTO
import com.vlad161.voicemessenger.other.BaseTest
import com.vlad161.voicemessenger.view.MessageView
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import java.io.File

class MessagePresenterImplTest : BaseTest() {

    private val view = mock(MessageView::class.java)

    private val dataRepository = mock(Model::class.java)

    private val mediaProvider = mock(MediaProvider::class.java)

    private lateinit var presenter: MessagePresenter

    @Before
    fun setUp() {
        presenter = MessagePresenterImpl(view, dataRepository, mediaProvider)
    }

    @Test
    fun onDestroyTest() {
        presenter.onDestroy()
        verify(dataRepository).clearFolder()
    }

    @Test
    fun stopRecordTest_500_ms() {
        presenter.stopRecord(500)

        verify(view, never()).addMessage(any())
        verify(view, never()).startLoad()

        verify(dataRepository, never()).sendVoiceMessage(any())
    }

    @Test
    fun stopRecordTest_1000_ms() {
        val file = File(getFileUri("1499279789754.3gp"))
        val messageDTO = MessageDTO(file.name, file.absolutePath, true)
        `when`(dataRepository.createFile()).thenReturn(file)
        `when`(dataRepository.sendVoiceMessage(any())).thenReturn(Single.just(messageDTO))

        presenter.startRecord()
        presenter.stopRecord(1000)

        verify(view, atLeastOnce()).addMessage(any())
        verify(view).startLoad()

        verify(dataRepository).sendVoiceMessage(any())
    }
}