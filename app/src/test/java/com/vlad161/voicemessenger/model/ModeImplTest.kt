package com.vlad161.voicemessenger.model

import com.vlad161.voicemessenger.model.api.ApiInterface
import com.vlad161.voicemessenger.model.dto.MessageDTO
import com.vlad161.voicemessenger.model.storage.FileManager
import com.vlad161.voicemessenger.other.BaseTest
import com.vlad161.voicemessenger.other.MEDIA_TYPE
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import java.io.File

class ModeImplTest : BaseTest() {

    private val apiInterface = mock(ApiInterface::class.java)

    private val fileManager = mock(FileManager::class.java)

    private lateinit var model: Model

    @Before
    fun setUp() {
        model = ModelImpl(apiInterface, fileManager, Schedulers.trampoline(), Schedulers.trampoline())
    }

    @Test
    fun sendVoiceTest() {
        val file = File(getFileUri("1499279789754.3gp"))
        val responseBody = ResponseBody.create(MediaType.parse(MEDIA_TYPE), file.readBytes())

        `when`(apiInterface.sendVoiceMessage(any())).thenReturn(Single.just(responseBody))
        `when`(fileManager.writeResponseFileToDisk(any())).thenReturn(file)
        val testObserver = model.sendVoiceMessage(file).test()

        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        assertEquals(testObserver.values()[0], MessageDTO(file.name, file.absolutePath, true))
    }

    @Test
    fun createFileTest() {
        model.createFile()
        verify(fileManager).createOutputFile()
    }

    @Test
    fun clearFolderTest() {
        model.clearFolder()
        verify(fileManager).clearFolder()
    }
}