package com.vlad161.voicemessenger.other

import com.vlad161.voicemessenger.BuildConfig
import org.junit.Ignore
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.net.URI

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class,
        sdk = intArrayOf(21))
@Ignore
open class BaseTest {

    protected fun getFileUri(fileName: String): URI {
        return javaClass.getResource("/files/$fileName").toURI()
    }

    protected fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    private fun <T> uninitialized(): T = null as T
}