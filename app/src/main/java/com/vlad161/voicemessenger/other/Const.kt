package com.vlad161.voicemessenger.other

const val UI_THREAD = "UI_THREAD"
const val IO_THREAD = "IO_THREAD"

const val BASE_URL = "http://192.168.1.63:5000/"

const val MEDIA_TYPE = "video/3gpp"
const val MULTIPART_BODY_NAME = "audio"
