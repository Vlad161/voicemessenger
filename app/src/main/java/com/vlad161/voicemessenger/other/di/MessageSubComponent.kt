package com.vlad161.voicemessenger.other.di

import com.vlad161.voicemessenger.other.di.scopes.Activity
import com.vlad161.voicemessenger.view.MessageActivity
import dagger.Subcomponent

@Activity
@Subcomponent(modules = arrayOf(MessageModule::class))
interface MessageSubComponent {
    fun inject(messageActivity: MessageActivity)
}