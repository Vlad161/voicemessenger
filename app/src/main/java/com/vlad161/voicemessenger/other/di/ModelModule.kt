package com.vlad161.voicemessenger.other.di

import android.content.Context
import com.vlad161.voicemessenger.model.api.ApiInterface
import com.vlad161.voicemessenger.model.api.ApiModule
import com.vlad161.voicemessenger.model.storage.FileManager
import com.vlad161.voicemessenger.model.storage.FileManagerImpl
import com.vlad161.voicemessenger.other.BASE_URL
import com.vlad161.voicemessenger.other.IO_THREAD
import com.vlad161.voicemessenger.other.UI_THREAD
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

@Module
class ModelModule {

    @Provides
    @Singleton
    fun provideApiInterface(): ApiInterface {
        return ApiModule.getApiInterface(BASE_URL)
    }

    @Provides
    @Singleton
    fun provideFileManager(context: Context): FileManager {
        return FileManagerImpl(context.filesDir.absolutePath)
    }

    @Provides
    @Singleton
    @Named(UI_THREAD)
    fun provideSchedulerUI() = AndroidSchedulers.mainThread()

    @Provides
    @Singleton
    @Named(IO_THREAD)
    fun provideSchedulerIO() = Schedulers.io()
}