package com.vlad161.voicemessenger.other.di

import com.vlad161.voicemessenger.model.Model
import com.vlad161.voicemessenger.other.di.scopes.Activity
import com.vlad161.voicemessenger.presenter.MediaProviderImpl
import com.vlad161.voicemessenger.presenter.MessagePresenter
import com.vlad161.voicemessenger.presenter.MessagePresenterImpl
import com.vlad161.voicemessenger.view.MessageView
import dagger.Module
import dagger.Provides

@Module
class MessageModule(val messageView: MessageView) {

    @Provides
    @Activity
    fun provideMainPresenter(dataRepository: Model): MessagePresenter {
        return MessagePresenterImpl(messageView, dataRepository, MediaProviderImpl())
    }
}