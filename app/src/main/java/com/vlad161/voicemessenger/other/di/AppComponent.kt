package com.vlad161.voicemessenger.other.di

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, ModelModule::class, PresenterModule::class))
interface AppComponent {
    fun plusMessageSubComponent(messageModule: MessageModule): MessageSubComponent
}