package com.vlad161.voicemessenger.other.di

import com.vlad161.voicemessenger.model.Model
import com.vlad161.voicemessenger.model.ModelImpl
import com.vlad161.voicemessenger.model.api.ApiInterface
import com.vlad161.voicemessenger.model.storage.FileManager
import com.vlad161.voicemessenger.other.IO_THREAD
import com.vlad161.voicemessenger.other.UI_THREAD
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named
import javax.inject.Singleton

@Module
class PresenterModule {

    @Provides
    @Singleton
    fun provideDataRepository(api: ApiInterface,
                              fileManager: FileManager,
                              @Named(UI_THREAD) uiScheduler: Scheduler,
                              @Named(IO_THREAD) ioScheduler: Scheduler): Model {
        return ModelImpl(api, fileManager, uiScheduler, ioScheduler)
    }
}