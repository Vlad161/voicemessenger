package com.vlad161.voicemessenger.other

import android.app.Application
import com.vlad161.voicemessenger.other.di.AppComponent
import com.vlad161.voicemessenger.other.di.AppModule
import com.vlad161.voicemessenger.other.di.DaggerAppComponent

class MyApplication : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        buildComponent()
    }

    private fun buildComponent() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(applicationContext)).build()
    }
}