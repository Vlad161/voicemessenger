package com.vlad161.voicemessenger.presenter

import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Build
import com.vlad161.voicemessenger.model.Model
import com.vlad161.voicemessenger.model.dto.MessageDTO
import com.vlad161.voicemessenger.view.MessageView
import io.reactivex.disposables.CompositeDisposable
import java.io.File
import java.lang.RuntimeException
import javax.inject.Inject

interface MessagePresenter {
    fun onStop()
    fun onDestroy()
    fun startRecord()
    fun stopRecord(duration: Long)
    fun startPlay(path: String)
    fun stopPlay()
    fun speedChanged(speed: Float)
}

class MessagePresenterImpl @Inject constructor(val view: MessageView,
                                               val dataRepository: Model,
                                               val mediaProvider: MediaProvider) : MessagePresenter {

    private val compositeDisposable = CompositeDisposable()

    private var recorder: MediaRecorder? = null

    private var player: MediaPlayer? = null

    private var recordedFile: File? = null

    private var playingSpeed = 1.0F


    override fun onStop() {
        compositeDisposable.clear()
    }

    override fun onDestroy() {
        dataRepository.clearFolder()
    }

    override fun startRecord() {
        recordedFile = dataRepository.createFile()
        recorder = mediaProvider.provideMediaRecorder(recordedFile?.path ?: return)
        recorder?.prepare()
        recorder?.start()
    }

    override fun stopRecord(duration: Long) {
        try {
            recorder?.stop()
        } catch(e: RuntimeException) {
            recordedFile?.delete()
            recordedFile = null
        } finally {
            recorder?.release()
            recorder = null
        }

        if (duration >= 1000) {
            recordedFile?.apply {
                val message = MessageDTO(this.name, this.path, false)
                view.addMessage(message)
                sendVoiceMessage(this)
            }
        }
        recordedFile = null
    }

    override fun startPlay(path: String) {
        if (player?.isPlaying ?: false) {
            stopPlay()
            view.playingCompleted()
        }

        player = mediaProvider.provideMediaPlayer(path)
        player?.setOnCompletionListener {
            stopPlay()
            view.playingCompleted()
        }
        player?.apply { activatePlayingSpeedIfPossible(this, playingSpeed) }
        player?.prepare()
        player?.start()
    }

    override fun stopPlay() {
        player?.pause()
        player?.release()
        player = null
    }

    override fun speedChanged(speed: Float) {
        playingSpeed = speed
        player?.apply { activatePlayingSpeedIfPossible(this, playingSpeed) }
    }

    private fun sendVoiceMessage(file: File) {
        view.startLoad()
        compositeDisposable.add(dataRepository.sendVoiceMessage(file).subscribe({
            view.addMessage(it)
        }, {
            view.showError(it.message.toString())
        }))
    }

    private fun activatePlayingSpeedIfPossible(player: MediaPlayer, speed: Float) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val playbackParams = player.playbackParams
            playbackParams.speed = speed
            player.playbackParams = playbackParams
        }
    }
}