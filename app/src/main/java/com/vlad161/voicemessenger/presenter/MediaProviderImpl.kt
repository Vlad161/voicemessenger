package com.vlad161.voicemessenger.presenter

import android.media.MediaPlayer
import android.media.MediaRecorder

interface MediaProvider {
    fun provideMediaRecorder(path: String): MediaRecorder
    fun provideMediaPlayer(path: String): MediaPlayer
}

class MediaProviderImpl : MediaProvider {

    override fun provideMediaRecorder(path: String): MediaRecorder {
        val recorder = MediaRecorder()
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC)
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        recorder.setOutputFile(path)
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
        return recorder
    }

    override fun provideMediaPlayer(path: String): MediaPlayer {
        val player = MediaPlayer()
        player.setDataSource(path)
        return player
    }
}