package com.vlad161.voicemessenger.model.dto

data class MessageDTO(val name: String, val path: String, val input: Boolean)