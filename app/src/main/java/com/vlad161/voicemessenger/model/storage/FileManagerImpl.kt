package com.vlad161.voicemessenger.model.storage

import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream

interface FileManager {
    fun writeResponseFileToDisk(inputStream: InputStream): File
    fun createOutputFile(): File
    fun clearFolder()
}

class FileManagerImpl(internalAbsolutePath: String) : FileManager {

    private val FOLDER_OUTPUT_NAME = "output_messages"

    private val FOLDER_INPUT_NAME = "input_messages"

    private val FILE_FORMAT = ".3gp"

    private val outputFileName: String
        get() = outputAbsolutePath + File.separator + System.currentTimeMillis() + FILE_FORMAT

    private val inputFileName: String
        get() = inputAbsolutePath + File.separator + System.currentTimeMillis() + FILE_FORMAT

    private var outputFolder: File

    private var inputFolder: File

    private var outputAbsolutePath: String

    private var inputAbsolutePath: String

    init {
        outputFolder = File(internalAbsolutePath + File.separator + FOLDER_OUTPUT_NAME)
        if (!outputFolder.exists()) {
            outputFolder.mkdir()
        }
        outputAbsolutePath = outputFolder.absolutePath

        inputFolder = File(internalAbsolutePath + File.separator + FOLDER_INPUT_NAME)
        if (!inputFolder.exists()) {
            inputFolder.mkdir()
        }
        inputAbsolutePath = inputFolder.absolutePath
    }

    override fun createOutputFile() = File(outputFileName)

    override fun clearFolder() {
        outputFolder.deleteRecursively()
        inputFolder.deleteRecursively()
    }

    override fun writeResponseFileToDisk(inputStream: InputStream): File {
        val file = createInputFile()

        var outputStream: OutputStream? = null

        try {
            val fileReader = ByteArray(4096)

            outputStream = FileOutputStream(file)

            while (true) {
                val read = inputStream.read(fileReader)

                if (read == -1) {
                    break
                }

                outputStream.write(fileReader, 0, read)
            }

            outputStream.flush()
        } finally {
            inputStream.close()
            outputStream?.close()
        }
        return file
    }

    private fun createInputFile() = File(inputFileName)
}