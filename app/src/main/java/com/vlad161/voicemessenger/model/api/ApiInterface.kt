package com.vlad161.voicemessenger.model.api

import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface ApiInterface {

    @Multipart
    @POST("echo/")
    fun sendVoiceMessage(@Part file: MultipartBody.Part): Single<ResponseBody>
}