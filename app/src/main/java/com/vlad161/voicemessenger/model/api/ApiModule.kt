package com.vlad161.voicemessenger.model.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

class ApiModule {

    companion object {

        private val loggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        fun getApiInterface(url: String): ApiInterface {
            val httpClient = OkHttpClient.Builder()
                    .addInterceptor(loggingInterceptor)
                    .build()

            val builder = Retrofit.Builder()
                    .baseUrl(url)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClient)

            return builder.build().create(ApiInterface::class.java)
        }
    }
}