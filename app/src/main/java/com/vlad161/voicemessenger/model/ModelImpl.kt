package com.vlad161.voicemessenger.model

import com.vlad161.voicemessenger.model.api.ApiInterface
import com.vlad161.voicemessenger.model.dto.MessageDTO
import com.vlad161.voicemessenger.model.storage.FileManager
import com.vlad161.voicemessenger.other.IO_THREAD
import com.vlad161.voicemessenger.other.UI_THREAD
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.SingleTransformer
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject
import javax.inject.Named

interface Model {
    fun sendVoiceMessage(uploadFile: File): Single<MessageDTO>
    fun createFile(): File
    fun clearFolder()
}

class ModelImpl @Inject constructor(val api: ApiInterface,
                                    val fileManager: FileManager,
                                    @Named(UI_THREAD) val uiScheduler: Scheduler,
                                    @Named(IO_THREAD) val ioScheduler: Scheduler) : Model {

    override fun sendVoiceMessage(uploadFile: File): Single<MessageDTO> {
        val requestBody = RequestBody.create(MediaType.parse("video/3gpp"), uploadFile)
        val multiPartBodyPart = MultipartBody.Part.createFormData("audio", uploadFile.name, requestBody)
        return api.sendVoiceMessage(multiPartBodyPart)
                .map {
                    val file = fileManager.writeResponseFileToDisk(it.byteStream())
                    MessageDTO(file.name, file.path, true)
                }
                .compose(applySchedulers())
    }

    override fun createFile() = fileManager.createOutputFile()

    override fun clearFolder() = fileManager.clearFolder()

    private fun <T> applySchedulers(): SingleTransformer<T, T> = SingleTransformer {
        it.subscribeOn(ioScheduler)
                .observeOn(uiScheduler)
    }
}