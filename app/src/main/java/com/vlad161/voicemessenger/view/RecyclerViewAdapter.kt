package com.vlad161.voicemessenger.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import com.vlad161.voicemessenger.R
import com.vlad161.voicemessenger.model.dto.MessageDTO
import java.text.SimpleDateFormat
import java.util.*

interface AdapterCallBack {
    fun startPlay(path: String)
    fun stopPlay()
}

class RecyclerViewAdapter(private val context: Context, private val callBack: AdapterCallBack) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    private val VIEW_TYPE_OUTPUT = 0

    private val VIEW_TYPE_INPUT = 1

    @LayoutRes
    private val resource = R.layout.item_message

    private val layoutInflate by lazy { LayoutInflater.from(context) }

    private val messages: MutableList<MessageDTO> = ArrayList()

    private val timeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())

    private var currentPlayingViewHolder: ViewHolder? = null


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val viewHolder = ViewHolder(layoutInflate.inflate(resource, parent, false))

        if (viewType == VIEW_TYPE_OUTPUT) {
            viewHolder.llContainer.gravity = Gravity.END
            (viewHolder.llContainer.layoutParams as FrameLayout.LayoutParams).gravity = Gravity.START
        } else {
            viewHolder.llContainer.gravity = Gravity.START
            (viewHolder.llContainer.layoutParams as FrameLayout.LayoutParams).gravity = Gravity.END
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val message = messages[position]
        holder.tvTime.text = convertFileNameToDateFormat(message.name)
        holder.imgBtnPlay.setOnClickListener {
            if (holder == currentPlayingViewHolder) {
                currentPlayingViewHolder = null
                callBack.stopPlay()
            } else {
                callBack.startPlay(message.path)
                currentPlayingViewHolder = holder
            }
            changeImageButtonSrc(holder)
        }
        changeImageButtonSrc(holder)
    }

    override fun getItemCount() = messages.size

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].input) VIEW_TYPE_INPUT else VIEW_TYPE_OUTPUT
    }

    fun addMessage(message: MessageDTO) {
        messages.add(message)
    }

    fun playingCompleted() {
        val completedPlayingViewHolder = currentPlayingViewHolder
        currentPlayingViewHolder = null
        completedPlayingViewHolder?.apply { changeImageButtonSrc(this) }
    }

    private fun changeImageButtonSrc(holder: ViewHolder) {
        val drawable: Drawable
        if (currentPlayingViewHolder == holder) {
            drawable = context.resources.getDrawable(android.R.drawable.ic_media_pause)
        } else {
            drawable = context.resources.getDrawable(android.R.drawable.ic_media_play)
        }
        holder.imgBtnPlay.setImageDrawable(drawable)
    }

    private fun convertFileNameToDateFormat(fileName: String): String {
        val time = fileName.split('.')[0].toLong()
        return timeFormat.format(Date(time))
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val llContainer = itemView.findViewById(R.id.ll_container) as LinearLayout
        val imgBtnPlay = itemView.findViewById(R.id.img_btn_play) as ImageButton
        val tvTime = itemView.findViewById(R.id.tv_time) as TextView
    }
}