package com.vlad161.voicemessenger.view.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.Button

interface CallBack {
    fun onPressed()
    fun onReleased(duration: Long)
}

class LongPressButton(context: Context, attrs: AttributeSet) : Button(context, attrs) {

    var callBack: CallBack? = null


    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                callBack?.onPressed()
                return true
            }

            MotionEvent.ACTION_UP -> {
                val duration = event.eventTime - event.downTime
                callBack?.onReleased(duration)
                return true
            }
        }
        return false
    }
}