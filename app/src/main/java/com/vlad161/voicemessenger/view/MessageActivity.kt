package com.vlad161.voicemessenger.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SwitchCompat
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ProgressBar
import com.vlad161.voicemessenger.R
import com.vlad161.voicemessenger.model.dto.MessageDTO
import com.vlad161.voicemessenger.other.MyApplication
import com.vlad161.voicemessenger.other.di.MessageModule
import com.vlad161.voicemessenger.presenter.MessagePresenter
import com.vlad161.voicemessenger.view.widgets.CallBack
import com.vlad161.voicemessenger.view.widgets.LongPressButton
import javax.inject.Inject

interface MessageView {
    fun startLoad()
    fun addMessage(message: MessageDTO)
    fun showError(error: String)
    fun playingCompleted()
}

class MessageActivity : AppCompatActivity(), MessageView {

    @Inject
    lateinit var presenter: MessagePresenter

    private val toolbar by lazy { findViewById(R.id.toolbar) as Toolbar }

    private val progressBar by lazy { findViewById(R.id.progress_bar) as ProgressBar }

    private val recyclerView by lazy { findViewById(R.id.recycler_view) as RecyclerView }

    private val button by lazy { findViewById(R.id.button) as LongPressButton }

    private val speedSwitch by lazy { findViewById(R.id.sw_speed) as SwitchCompat }

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter

    private val REQUEST_RECORD_AUDIO_CODE = 101


    init {
        MyApplication.appComponent.plusMessageSubComponent(MessageModule(this)).inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        checkPermission()

        recyclerViewAdapter = RecyclerViewAdapter(this, adapterCallback)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = recyclerViewAdapter

        button.callBack = longPressButtonCallback
        speedSwitch.setOnCheckedChangeListener { _, isChecked ->
            presenter.speedChanged(if (isChecked) 1.5F else 1.0F)
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_RECORD_AUDIO_CODE && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            checkPermission()
        }
    }

    override fun startLoad() {
        progressBar.visibility = View.VISIBLE
    }

    override fun addMessage(message: MessageDTO) {
        progressBar.visibility = View.INVISIBLE
        recyclerViewAdapter.addMessage(message)
        recyclerViewAdapter.notifyItemInserted(recyclerViewAdapter.itemCount - 1)
    }

    override fun showError(error: String) {
        progressBar.visibility = View.INVISIBLE
        print(error)
    }

    override fun playingCompleted() {
        recyclerViewAdapter.playingCompleted()
    }

    private val longPressButtonCallback = object : CallBack {
        override fun onPressed() {
            presenter.startRecord()
        }

        override fun onReleased(duration: Long) {
            presenter.stopRecord(duration)
        }
    }

    private val adapterCallback = object : AdapterCallBack {
        override fun startPlay(path: String) {
            presenter.startPlay(path)
        }

        override fun stopPlay() {
            presenter.stopPlay()
        }
    }

    private fun checkPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECORD_AUDIO), REQUEST_RECORD_AUDIO_CODE)
    }
}
